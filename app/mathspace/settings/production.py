from .defaults import *

DEBUG = False

TEMPLATE_DEBUG = False

ALLOWED_HOSTS = [
    '.gontro.com',  # Allow domain and subdomains
    '.gontro.com.',  # Also allow FQDN and subdomains
]

SECRET_KEY = 'x!wc!u2yx9_hg4p!ur3xem)^z^d!vydy(z72bik=v4_orltm(('
