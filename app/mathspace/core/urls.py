from django.conf.urls import patterns, url

urlpatterns = patterns('',
    url(r'^api/astar', 'mathspace.core.views.astar', name='astar')
)
