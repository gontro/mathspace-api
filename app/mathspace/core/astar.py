#!/usr/bin/python

import heapq


class Tile(object):
    def __init__(self, x, y, cost):
        self.x = x
        self.y = y
        self.cost = cost
        self.parent = None
        self.heuristic = 0
        self.total_cost = 0


class AStar(object):
    def __init__(self):
        self.opened = []
        heapq.heapify(self.opened)
        self.closed = set()
        self.tiles = []
        self.restrict_movement = False
        self.grid_height = 0
        self.grid_width = 0
        self.start = None
        self.end = None

    def init_grid(self, grid):
        self.grid_height = len(grid)
        self.grid_width = len(grid[0])
        for x in range(self.grid_width):
            for y in range(self.grid_height):
                self.tiles.append(Tile(x, y, grid[y][x]))
        self.start = self.get_tile(0, 0)
        self.end = self.get_tile(self.grid_width - 1, self.grid_height - 1)

    def get_tile(self, x, y):
        # get tile from list based on coords
        return self.tiles[x * self.grid_height + y]

    def get_neighbor_tiles(self, tile):
        # get ajacent tiles and account for restricted movement
        tiles = []
        if tile.x < self.grid_width - 1:
            tiles.append(self.get_tile(tile.x + 1, tile.y))
        if tile.y < self.grid_height - 1:
            tiles.append(self.get_tile(tile.x, tile.y + 1))
        if tile.x > 0 and not self.restrict_movement:
            tiles.append(self.get_tile(tile.x - 1, tile.y))
        if tile.y > 0 and not self.restrict_movement:
            tiles.append(self.get_tile(tile.x, tile.y - 1))
        return tiles

    def update_tile(self, neighbor_tile, tile):
        # get rough cost of distance to end tile
        neighbor_tile.heuristic = abs(neighbor_tile.x - self.end.x) + abs(neighbor_tile.y - self.end.y)
        neighbor_tile.total_cost = neighbor_tile.heuristic + neighbor_tile.cost
        neighbor_tile.parent = tile

    def compile_path(self, tile, path=None):
        if path is None:
            path = []
        path.append((tile.x, tile.y))
        if tile.parent:
            return self.compile_path(tile.parent, path)
        else:
            return path

    def process(self, restrict_movement=False):
        self.restrict_movement = restrict_movement
        heapq.heappush(self.opened, (self.start.total_cost, self.start))
        while len(self.opened):
            total_cost, tile = heapq.heappop(self.opened)
            self.closed.add(tile)
            # if target tile, compile and return path taken
            if tile is self.end:
                return self.compile_path(self.end)
            neighbor_tiles = self.get_neighbor_tiles(tile)
            for neighbor_tile in neighbor_tiles:
                if neighbor_tile not in self.closed:
                    if (neighbor_tile.total_cost, neighbor_tile) in self.opened:
                        # check if current path is better than previous ones
                        if neighbor_tile.cost > tile.cost:
                            self.update_tile(neighbor_tile, tile)
                    else:
                        self.update_tile(neighbor_tile, tile)
                        heapq.heappush(self.opened, (neighbor_tile.total_cost, neighbor_tile))

#
#astar = AStar()
#
#astar.init_grid([
#    [1, 9, 9, 9, 9],
#    [1, 9, 9, 9, 9],
#    [1, 9, 1, 1, 1],
#    [1, 1, 1, 9, 1]
#])
#print(astar.process(True))