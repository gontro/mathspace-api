import json
from django.http import JsonResponse

from .astar import AStar

def astar(request):
    data = json.loads(request.body)
    pathing = AStar()
    pathing.init_grid(data['grid'])
    path = pathing.process(data['restrict'])
    return JsonResponse({
        'path': path
    })