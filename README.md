Mathspace Technical Challenge - API
===================================

A solution to problem #1 in the Mathspace Technical Challenge.

## Technology stack

* Django
* Redis

## Getting up and running

This setup was built on a Ubuntu 14.04.1 server.  The following will get you setup to develop.

```
$ git clone git@bitbucket.org:gontro/mathspace-api.git
$ cd mathspace-api
$ sudo aptitude install build-essential python-dev redis-server
$ sudo pip install virtualenv
$ virtualenv --no-site-packages env
$ ./env/bin/pip install -r requirements.txt
```

Create app/mathspace/settings/local.py and add `from [development|production] import *`

To run the server, execute this command.

```
$ cd mathspace-api/app
$ ./manage.py runserver
```

And that's it!  Browse to http://localhost:8000 to access.
